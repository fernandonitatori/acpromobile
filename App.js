import React from 'react';
import { View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { Context, Provider } from "./components/globalContext/globalContext.js";
import Navigator from './components/navigation/navigator.js';
import Home from './src/telas/Home.js';

export default function App(props) {
  return (
    <Provider>
      <View style={{flex:1}}>
        <NavigationContainer>
            <Navigator />
        </NavigationContainer>  
      </View>

    </Provider>
  );
};
