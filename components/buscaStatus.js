import React from "react";

export async function buscaStatus (idstatus) {
    try {
        const resultado = await fetch(`http://acpro-sescv2.herokuapp.com/api/v2/locacoes/${idstatus}`, {
                                        method: 'GET',
                                        headers: {
                                            'Authorization': 'Token ddd22b3adf2cf20bca7b5b917fa6392feddb5339',  
                                            'Content-Type': 'application/json'
                                        },
                                })
                                .then( res => res.json())
                                .then( jsonRes => setLocacoes(jsonRes))
                                .catch( error => console.log('error'));
        return resultado.data[0]                        

    }
    catch (error) {
        console.log(error);
    }


}