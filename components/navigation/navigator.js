import React, { useContext, useEffect }  from "react";
import { createStackNavigator } from '@react-navigation/stack';

import Home from '../../src/telas/Home.js';
import Login from '../../src/telas/Login.js';
import Processos from '../../src/telas/Processos.js';
import Sobre from '../../src/telas/Sobre.js';
import Locacoes from '../../src/telas/Locacoes.js';
import Aquisicoes from '../../src/telas/Aquisicoes.js';
import Manutencoes from '../../src/telas/Manutencoes.js';
import DetalheLocacao from "../../src/telas/detalheLocacao.js";
import DetalheAquisicao from "../../src/telas/detalheAquisicao.js";
import DetalheManutencao from "../../src/telas/detalheManutencao.js";
import { Context, Provider } from '../../components/globalContext/globalContext.js';



const Stack = createStackNavigator();

function Navigator(props) {

    
    const globalContext = useContext(Context)
    const { isLoggedIn } = globalContext;

    return (
        <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={Home}  options={{ headerShown: false }} />
                <Stack.Screen name="Login" component={Login}  options={{ headerShown: false }} />
                <Stack.Screen name="Sobre" component={Sobre}  options={{ headerShown: false }} />
                <Stack.Screen name="Processos" component={Processos}  options={{ headerShown: false }} />
                <Stack.Screen name="Locacoes" component={Locacoes}  options={{ headerShown: false }} />    
                <Stack.Screen name="Aquisicoes" component={Aquisicoes}  options={{ headerShown: false }} />
                <Stack.Screen name="Manutencoes" component={Manutencoes}  options={{ headerShown: false }} />
                <Stack.Screen name="DetalheLocacao" component={DetalheLocacao}  />
                <Stack.Screen name="DetalheAquisicao" component={DetalheAquisicao}  />
                <Stack.Screen name="DetalheManutencao" component={DetalheManutencao}  />

        </Stack.Navigator>

    )
 

}

export default Navigator;