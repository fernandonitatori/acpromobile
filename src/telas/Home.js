import React, { useContext, useEffect } from 'react';
import { Text, StyleSheet, Dimensions, View, Image, TouchableOpacity} from 'react-native';
import { Context, Provider } from '../../components/globalContext/globalContext.js'
import topo from '../../assets/topo.png';
import OneSignal from 'react-native-onesignal';


// 473c152c-7766-4691-9e44-9bbd0e67de05

const width = Dimensions.get('screen').width;

const Separator = () => (
    <View style={estilos.separator} />
  );

export default function Home ({ navigation, route, props }) {

    OneSignal.setAppId('473c152c-7766-4691-9e44-9bbd0e67de05');
    
    OneSignal.promptForPushNotificationsWithUserResponse();

    OneSignal.setNotificationWillShowInForegroundHandler(notificationReceivedEvent => {
         console.log("OneSignal: notification will show in foreground:", notificationReceivedEvent);
        let notification = notificationReceivedEvent.getNotification();
        console.log("notification: ", notification);
        const data = notification.additionalData
        console.log("additionalData: ", data);
        notificationReceivedEvent.complete(notification);
    });

    OneSignal.setNotificationOpenedHandler(notification => {
        console.log("OneSignal: notification opened:", notification);
    });
    
    
    const globalContext = useContext(Context)
    const { usuario, isLoggedIn, setIsLoggedIn } = globalContext;

    var loginButton;
    var button2;
    var pagina;
    var pagina2;
    var painel;
    
    const logout = () => {
        setIsLoggedIn(false);    
        navigation.navigate("Home");
    }

    if (isLoggedIn) {
        loginButton = <Text style={estilos.nome_botao}> SISTEMA </Text> ;
        button2 = <Text style={estilos.nome_botao}> LOGOUT </Text> ;
        pagina = <TouchableOpacity style={estilos.cartao} onPress={() => navigation.navigate("Processos")} >
                        {loginButton}   
                  </TouchableOpacity>
        pagina2 =  <TouchableOpacity style={estilos.cartao} onPress={() => logout ()} >
                        {button2}   
                    </TouchableOpacity>
        painel = <Text style={estilos.fixToText}> Olá, {usuario} </Text>
    } else {
        loginButton = <Text style={estilos.nome_botao}> LOGIN </Text>;
        button2 = <Text style={estilos.nome_botao}> SOBRE </Text> ;
        
        pagina =   <TouchableOpacity style={estilos.cartao} onPress={() => navigation.navigate("Login")} >
                    {loginButton}   
                   </TouchableOpacity>
        pagina2 =   <TouchableOpacity style={estilos.cartao} onPress={() => navigation.navigate("Sobre")} >
                        {button2}   
                    </TouchableOpacity>
        painel = <Text style={estilos.fixToText}>Efetue login no sistema para acompanhar os processos:</Text>            

    }

   
    return <>
            
                <Image source={topo} style={estilos.topo}/>
                <Text style={estilos.titulo}>SESC - Setor Audiovisual</Text>
                
                <TouchableOpacity style={estilos.cartao_nome} disabled>
                    <Text style={estilos.nome}> AcPro Mobile </Text>
                </TouchableOpacity>

                
                {painel}

                {pagina}
                
                {pagina2}
                
                
            
    </>
};

const estilos = StyleSheet.create ({
    topo: {
        width: "100%",
        height: 578 / 1035 * width, 
    },
    titulo: {
        position: "absolute",
        width: "100%",
        textAlign: "left",
        fontSize: 16,
        lineHeight: 26,
        color: "black",
        fontWeight: "bold",
        padding: 30
    },
    nome: {
        marginVertical: 8,
        width: "100%",
        textAlign: "center",
        fontSize: 30,
        fontWeight: "bold",
        lineHeight: 40,
        color: "black"
    },
    fixToText: {
        marginVertical: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: "#FFFFFFFF",
        textAlign: "center",
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: 3,
    },
    image: {
        flex: 1,
        justifyContent: "center"
    },
    cartao: {    
        backgroundColor: 'white',
        justifyContent: 'center',
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 5,
        flexDirection: "row",

         // Android
         elevation: 4,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,
    },
    cartao_nome: {    
        backgroundColor: 'white',
        justifyContent: 'center',
        marginVertical: 8,
        marginHorizontal: 16,
        flexDirection: "row",

         // Android
         elevation: 20,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,
    },
    
    nome_botao: {
        fontSize: 16,
        lineHeight: 22,
        fontWeight: 'bold',
        justifyContent: 'center',
        textAlign: "center",
        marginVertical: 8,
    },
    nome_sistema: {
        fontSize: 16,
        width: "100%",
        lineHeight: 22,
        fontWeight: 'bold',
        justifyContent: 'center',
        textAlign: "center",
        marginVertical: 8,
    },
});


