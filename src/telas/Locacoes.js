import React, {useState, useEffect} from 'react';
import { Button, FlatList , Text, StyleSheet, Dimensions, View, TouchableOpacity  } from 'react-native';
import topo from '../../assets/topo.png';
import { FlashList } from "@shopify/flash-list";


const width = Dimensions.get('screen').width;

const Separator = () => (
    <View style={estilos.separator} />
  );

export default function Locacoes ({ navigation, route, props }) {

    const [locacoes, setLocacoes] = useState([]);
    const [statuses, setStatus] = useState([]);
    
    useEffect( () => {
                 fetch(`https://acpro-sescv2.herokuapp.com/api/v2/locacoes`, {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Token ddd22b3adf2cf20bca7b5b917fa6392feddb5339',  
                        'Content-Type': 'application/json'
                    },
                    })
                    .then( res => res.json())
                    .then( jsonRes => setLocacoes(jsonRes))
                    .catch( error => console.log('error'));
                }, [])

                

    useEffect( () => {
                  fetch(`https://acpro-sescv2.herokuapp.com/api/v2/status/`, {
                    method: 'GET',
                    headers: {
                        'Authorization': 'Token ddd22b3adf2cf20bca7b5b917fa6392feddb5339',  
                        'Content-Type': 'application/json'
                    },
                    })
                    .then( res => res.json())
                    .then( jsonRes => setStatus(jsonRes))
                    .catch( error => console.log('error'));
                    // console.log(status);        
                    }, [])            
    
            
    return <>
        <View style={estilos.outerPage}>
            <View>
                <View style={estilos.home}>
                    <TouchableOpacity style={estilos.nome_home} onPress={() => navigation.navigate("Processos")}>
                        <Text style={estilos.nome_botao} >Voltar</Text>
                    </TouchableOpacity>    
                    <Text style={estilos.nome}> Locações </Text>
                </View>    
                <FlashList
                    data={locacoes}
                    keyExtractor={locacoes => locacoes.id}
                    initialNumToRender={5}
                    estimatedItemSize={3139}
                    renderItem = {({ item }) => (
                        <View style={estilos.cartao} >
                             
                            <Text key={item.id} style={estilos.textid}>{item.id}</Text>
                            <Text style={estilos.textdescricao}>{item.descricao}</Text>
                            <Text style={estilos.textdescricao}>Status: {item.status.descricao}</Text>
                            <Button onPress={() => navigation.navigate("DetalheLocacao",{item})} title='Detalhes' />
                            <Text style={estilos.textdescricao}>Solicitado em {item.data_cadastro}</Text>    
                        </View>
                    )}

                            
                />

            </View>
            
        </View>
            
    </>
};

const estilos = StyleSheet.create ({
    cartao: {    
        backgroundColor: 'white',
        justifyContent: 'center',
        marginVertical: 5,
        marginHorizontal: 5,
        borderRadius: 5,
    //    width: "100%",
    //    flexDirection: "row",

         // Android
         elevation: 10,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,
    },
    nome: {
        marginVertical: 30,
        width: "70%",
        textAlign: "center",
        fontSize: 30,
        fontWeight: "bold",
        lineHeight: 40,
        color: "black"
    },
    home: {
        marginVertical: 30,
        width: "100%",
        textAlign: "center",
        fontSize: 16,
        fontWeight: "bold",
        lineHeight: 16,
        color: "white",
        flexDirection: "row"
    },
    nome_home: {
        marginVertical: 30,
        marginLeft: 10, 
        width: "20%",
        textAlign: "center",
        fontSize: 16,
        fontWeight: "bold",
        lineHeight: 16,
        flexDirection: "row"
    },
    formbox: {
        width: "100%",
        height: "100%",
        backgroundColor: '#f5f5f5',
        margin: 0,
        borderRadius: 15,
        alignItems: "center",
        padding: 5,
            
    },
    outerPage: {
        backgroundColor: "white",
        color: "#000000",
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        alignItems: "center",
        justifyContent: "center",
        padding: 0,
        margin: 0,
    },
    textdescricao: {
        fontSize: 12,
        textAlign: "justify",
        margin: 10,
        marginLeft: 20,
        marginHorizontal: 10,
    },
    textstatus: {
        fontSize: 14,
        textAlign: "justify",
        fontWeight: "bold",
        margin: 10,
        marginLeft: 20,
        marginHorizontal: 10,
        color: "blue",
    },

     textid: {
        fontSize: 18,
        textAlign: "justify",
        margin: 5,
        marginHorizontal: 20,
        fontWeight: "bold",
    },
    nome_botao: {
        fontSize: 16,
        lineHeight: 22,
        fontWeight: 'bold',
        width: "100%",
        justifyContent: 'center',
        textAlign: "center",
        marginVertical: 8,
        borderRadius: 5,
        flexDirection: "row",
        backgroundColor: "white",
        color: "black",

         // Android
         elevation: 10,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,
    },
    botao: {
        backgroundColor: '#F5F5F5',
        justifyContent: 'center',
        width: "20%",
        marginVertical: 10,
        marginHorizontal: 16,
        borderRadius: 5,
        flexDirection: "row",

         // Android
         elevation: 4,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,        
    },    
});
