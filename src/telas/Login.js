import React, {useState, useEffect, useContext } from 'react';
import { Alert, Text, StyleSheet, Dimensions, View, Image, TouchableOpacity} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import topo from '../../assets/topo.png';
import { Context, Provider } from '../../components/globalContext/globalContext.js'


const width = Dimensions.get('screen').width;

const Separator = () => (
    <View style={estilos.separator} />
  );

export default function Login ({ navigation, route, props }) {

    const [ username, setUsername ] = useState(""); 
    const [ password, setPassword ] = useState(""); 

    const globalContext = useContext(Context);
    const { setUsuario, isLoggedIn, setIsLoggedIn } = globalContext;

    
    const auth = async () => {

        await fetch('https://acpro-sescv2.herokuapp.com/auth/', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({ username, password })
            })
            
            .then( res => {
                if (res.ok) {
                    return res.json()     
                }
                else {
                    throw res.json()
                    console.log("Falhow json")
                }
            })
            .then( json => {
                 console.log("LOGADO");
                 setIsLoggedIn(true);
                 setUsuario(username);
                 navigation.navigate("Processos", {user: username});
                 
            })
            .catch( error => {
                console.log('error');
                Alert.alert("Usuário ou Senha incorreta.");
            })

    }              

    

    return <>
        
            <View style={estilos.outerPage}>
                <View style={estilos.formbox}>
                    <Text style={estilos.nome}> LOGIN </Text> 
                    <Text style={estilos.textLabel}>Usuário:</Text>
                    <TextInput style={estilos.textInput} autoCapitalize='none' placeholder='Usuário' onChangeText={ text => setUsername(text)} value={username} />
                    <Text style={estilos.textLabel}>Senha:</Text>
                    <TextInput style={estilos.textInput} autoCapitalize={'none'} secureTextEntry={true} placeholder='Digite a senha' onChangeText={ text => setPassword(text)} value={password} />
                    <TouchableOpacity style={estilos.cartao} onPress={() => auth ()}>
                        <Text style={estilos.nome_botao}> Acessar </Text>
                    </TouchableOpacity>    
                </View>
                
            </View>
        
    </>
};

const estilos = StyleSheet.create ({
       nome: {
        marginVertical: 30,
        width: "100%",
        textAlign: "center",
        fontSize: 30,
        fontWeight: "bold",
        lineHeight: 40,
        color: "black"
    },
    cartao: {    
        backgroundColor: 'white',
        justifyContent: 'center',
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 5,
        flexDirection: "row",

         // Android
         elevation: 4,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,
    },
    
    nome_botao: {
        fontSize: 16,
        lineHeight: 22,
        fontWeight: 'bold',
        justifyContent: 'center',
        textAlign: "center",
        marginVertical: 8,
    },
    formbox: {
        width: "75%",
        height: "75%",
        backgroundColor: '#f5f5f5',
        margin: 0,
        borderRadius: 15,
        alignItems: "center",
        padding: 10,
            
    },
    outerPage: {
        backgroundColor: "#d3d3d3",
        color: "#000000",
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        alignItems: "center",
        justifyContent: "center",
        padding: 0,
        margin: 0,
    },
    textInput: {
        width: "100%",
        backgroundColor: "white",
        margin: 10,
        elevation: 10,
        
    },
    textLabel: {
        width: "100%",
        fontWeight: 'bold',
        padding: 0,        
    },
});


