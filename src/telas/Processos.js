import React , { useContext, useEffect } from 'react';
import { Button, Text, StyleSheet, Dimensions, View, Image, TouchableOpacity} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import topo from '../../assets/topo.png';
import { Context } from '../../components/globalContext/globalContext.js'
import { BackHandler} from 'react-native';



const width = Dimensions.get('screen').width;

const Separator = () => (
    <View style={estilos.separator} />
  );

export default function Processos ({ navigation, route, props }) {

    useEffect(() => {
        const backHandler = BackHandler.addEventListener('hardwareBackPress', () => true)
        return () => backHandler.remove()
    }, [])

    const globalContext = useContext(Context)
    const { usuario, setUsuario, isLoggedIn, setIsLoggedIn } = globalContext;
   
    
    
    
    return <>
        
        <View style={estilos.outerPage}>
            <View style={estilos.formbox}>
                
                <Text style={estilos.nome}> Solicitações </Text>
                <TouchableOpacity style={estilos.cartao} onPress={() => navigation.navigate("Locacoes")}>
                    <Text style={estilos.nome_botao}> Locações </Text>
                </TouchableOpacity>
                <TouchableOpacity style={estilos.cartao} onPress={() => navigation.navigate("Aquisicoes")}>
                    <Text style={estilos.nome_botao}> Aquisições </Text>
                </TouchableOpacity>
                <TouchableOpacity style={estilos.cartao} onPress={() => navigation.navigate("Manutencoes")}>
                    <Text style={estilos.nome_botao}> Manutenções </Text>
                </TouchableOpacity>
                <TouchableOpacity style={estilos.botao} onPress={() => navigation.navigate("Home")}>
                    <Text style={estilos.nome_botao}> Home </Text>
                </TouchableOpacity>    
            </View>
            
        </View>
            
    </>
};

const estilos = StyleSheet.create ({
    
    nome: {
        marginVertical: 30,
        width: "100%",
        textAlign: "center",
        fontSize: 30,
        fontWeight: "bold",
        lineHeight: 40,
        color: "black"
    },
    cartao: {    
        backgroundColor: 'white',
        justifyContent: 'center',
        marginVertical: 20,
        marginHorizontal: 16,
        borderRadius: 5,
        flexDirection: "row",

         // Android
         elevation: 4,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,
    },
    
    nome_botao: {
        fontSize: 16,
        width: "100%",
        lineHeight: 22,
        fontWeight: 'bold',
        justifyContent: 'center',
        textAlign: "center",
        marginVertical: 8,
    },
    
    formbox: {
        width: "90%",
        height: "90%",
        backgroundColor: '#f5f5f5',
        margin: 0,
        borderRadius: 15,
        alignItems: "center",
        padding: 10,
    },
    outerPage: {
        backgroundColor: "#d3d3d3",
        color: "#000000",
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        alignItems: "center",
        justifyContent: "center",
        padding: 0,
        margin: 0,
    },
    botao: {
        backgroundColor: '#F5F5F5',
        justifyContent: 'center',
        width: "20%",
        marginVertical: 90,
        marginHorizontal: 16,
        borderRadius: 5,
        flexDirection: "row",

         // Android
         elevation: 4,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,        
    },
});


