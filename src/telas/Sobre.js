import React from 'react';
import { Text, StyleSheet, Dimensions, View, Image, TouchableOpacity} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import topo from '../../assets/topo.png';


const width = Dimensions.get('screen').width;

const Separator = () => (
    <View style={estilos.separator} />
  );

export default function Sobre () {

    return <>
        <View style={estilos.outerPage}>
            <View style={estilos.formbox}>
                <Text style={estilos.nome}> AcPro Mobile</Text>
                <TouchableOpacity style={estilos.cartao} disabled>
                    <Text style={estilos.nome_botao}> O sistema AcPro Mobile permite acompanhar os processos de Locação, Aquisição e Manutenção do Setor na tela do seu dispositivo móvel.</Text>
                </TouchableOpacity>
            </View>
        </View>
    </>
};

const estilos = StyleSheet.create ({
    nome: {
        marginVertical: 30,
        width: "100%",
        textAlign: "center",
        fontSize: 30,
        fontWeight: "bold",
        lineHeight: 40,
        color: "black"
    },
    cartao: {    
        backgroundColor: 'white',
        justifyContent: 'center',
        marginVertical: 20,
        marginHorizontal: 5,
        borderRadius: 5,
     //   flexDirection: "row",

         // Android
         elevation: 4,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,
    },
    formbox: {
        width: "75%",
        height: "75%",
        backgroundColor: '#f5f5f5',
        margin: 0,
        borderRadius: 15,
        alignItems: "center",
        padding: 10,
        
            
    },
    outerPage: {
        backgroundColor: "#d3d3d3",
        color: "#000000",
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        alignItems: "center",
        justifyContent: "center",
        padding: 0,
        margin: 0,
    },
    nome_botao: {
        fontSize: 14,
        lineHeight: 18,
        // fontWeight: 'bold',
        justifyContent: 'center',
        textAlign: "justify",
        marginVertical: 8,
        marginHorizontal: 10, 
    },
});


