import React, {useState, useEffect} from 'react';
import { Button, FlatList , Text, StyleSheet, Dimensions, View, TouchableOpacity  } from 'react-native';
import topo from '../../assets/topo.png';



const width = Dimensions.get('screen').width;

export default function DetalheManutencao ({ navigation, route, props }) {

    const { item } = route.params;
    // console.log(item);
    

    return <>
        <View style={estilos.outerPage}>
            <View style={estilos.formbox}>
                
                <Text style={estilos.nome}>Manutenção: {item.id}</Text>
                <View style={estilos.cartao}>
                    <View>
                        <Text style={estilos.textlabel}>Descrição:</Text>
                        <Text style={estilos.textfield}>{item.descricao}</Text>
                    </View>
                    <View>
                        <Text style={estilos.textlabel}>Memorial:</Text>
                        <Text style={estilos.textfield}>{item.memorial.descricao}</Text>
                    </View>
                    <View>
                        <Text style={estilos.textlabel}>Status: </Text>
                        <Text style={estilos.textfield}>{item.status.descricao}</Text>
                    </View>
                    <View>
                        <Text style={estilos.textlabel}>Data: </Text>
                        <Text style={estilos.textfield}>{item.data_cadastro}</Text>
                    </View>    
                </View>
            </View>
            
        </View>
            
    </>
};

const estilos = StyleSheet.create ({
    cartao: {    
        backgroundColor: 'white',
        justifyContent: 'center',
        marginVertical: 5,
        marginHorizontal: 5,
        borderRadius: 5,
    //    flexDirection: "row",

         // Android
         elevation: 10,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,
    },
    nome: {
        marginVertical: 30,
        width: "70%",
        textAlign: "center",
        fontSize: 30,
        fontWeight: "bold",
        lineHeight: 40,
        color: "black"
    },
    home: {
        marginVertical: 30,
        width: "100%",
        textAlign: "center",
        fontSize: 16,
        fontWeight: "bold",
        lineHeight: 16,
        color: "white",
        flexDirection: "row"
    },
    nome_home: {
        marginVertical: 30,
        marginLeft: 10, 
        width: "20%",
        textAlign: "center",
        fontSize: 16,
        fontWeight: "bold",
        lineHeight: 16,
        flexDirection: "row"
    },
    formbox: {
        width: "90%",
        height: "90%",
        backgroundColor: '#f5f5f5',
        margin: 0,
        borderRadius: 15,
        alignItems: "center",
        padding: 10,
            
    },
    outerPage: {
        backgroundColor: "#d3d3d3",
        color: "#000000",
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        alignItems: "center",
        justifyContent: "center",
        padding: 0,
        margin: 0,
    },
    
    textdescricao: {
        fontSize: 12,
        textAlign: "justify",
        margin: 10,
        marginLeft: 20,
        marginHorizontal: 10,
    },
    textstatus: {
        fontSize: 14,
        textAlign: "justify",
        fontWeight: "bold",
        margin: 10,
        marginLeft: 20,
        marginHorizontal: 10,
        color: "blue",
    },
    textlabel: {
        fontSize: 14,
        textAlign: "justify",
        fontWeight: "bold",
        marginLeft: 10,
        margin: 20,
       // marginBottom: 2,
        marginHorizontal: 10,
        color: "blue",
    },
    textfield: {
        fontSize: 13,
        textAlign: "justify",
        marginBottom: 20,
        marginLeft: 10,
        marginHorizontal: 10,
        color: "#000",
    },

    textid: {
        fontSize: 18,
        textAlign: "justify",
        margin: 5,
        marginHorizontal: 20,
        fontWeight: "bold",
    },
    nome_botao: {
        fontSize: 16,
        lineHeight: 22,
        fontWeight: 'bold',
        width: "100%",
        justifyContent: 'center',
        textAlign: "center",
        marginVertical: 8,
        borderRadius: 5,
        flexDirection: "row",
        backgroundColor: "white",
        color: "black",

         // Android
         elevation: 10,

         // iOS
         shadowColor: '#000',
         shadowOffset: {
             width: 0,
             height: 2,
         },
         // shadowOpacity: 0.23,
         shadowRadius: 2.62,
    },
    
});


